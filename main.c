#include <windows.h>
#include <string.h>
#include <stdio.h>


// Global variable
HINSTANCE hInst;
POINT position;
POINT mouse;

// Function prototypes.
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int);
void onPaint(HWND);
LRESULT CALLBACK MainWndProc(HWND, UINT, WPARAM, LPARAM);


// Application entry point. This is the same as main() in standart C.
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    MSG msg;
    BOOL bRet;
    WNDCLASS wcx;          // register class
    HWND hWnd;

    hInst = hInstance;     // Save the application-instance handle.
        // Fill in the window class structure with parameters that describe the main window.

    wcx.style = CS_HREDRAW | CS_VREDRAW;              // redraw if size changes
    wcx.lpfnWndProc = (WNDPROC) MainWndProc;                    // points to window procedure
    wcx.cbClsExtra = 0;                               // no extra class memory
    wcx.cbWndExtra = 0;                               // no extra window memory
    wcx.hInstance = hInstance;                        // handle to instance
    wcx.hIcon = LoadIcon(NULL, IDI_APPLICATION);      // predefined app. icon
    wcx.hCursor = LoadCursor(NULL, IDC_ARROW);                                   // predefined arrow
    wcx.hbrBackground = GetStockObject(WHITE_BRUSH);  // white background brush
    wcx.lpszMenuName =  (LPCSTR) "MainMenu";                   // name of menu resource
    wcx.lpszClassName = (LPCSTR) "MainWClass";                 // name of window class

    // Register the window class.

    if (!RegisterClass(&wcx)) return FALSE;

    // create window of registered class

    hWnd = CreateWindow(
        "MainWClass",        // name of window class
        "ITU",       // title-bar string
        WS_OVERLAPPEDWINDOW, // top-level window
        50,                  // default horizontal position
        100,                 // default vertical position
        640,                 // default width
        480,                 // default height
        (HWND) NULL,         // no owner window
        (HMENU) NULL,        // use class menu
        hInstance,           // handle to application instance
        (LPVOID) NULL);      // no window-creation data
    if (!hWnd) return FALSE;

    // Show the window and send a WM_PAINT message to the window procedure.
    // Record the current cursor position.

    ShowWindow(hWnd, nCmdShow);
    UpdateWindow(hWnd);

    // loop of message processing

    position.x = 0;
    position.y = 0;

    while( (bRet = GetMessage( &msg, NULL, 0, 0 )) != 0)
    {
        if (bRet == -1)
        {
            // handle the error and possibly exit
        }
        else
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }
    return (int) msg.wParam;
}

// **************************************************************** //
LRESULT CALLBACK MainWndProc(
    HWND hWnd,        // handle to window
    UINT uMsg,        // message identifier
    WPARAM wParam,    // first message parameter
    LPARAM lParam)    // second message parameter
{

    switch (uMsg)
    {
        case WM_CREATE:
            // Initialize the window.
            return 0;

        case WM_SIZE:
            // Set the size and position of the window.
            return 0;

        case WM_DESTROY:
            // Clean up window-specific data objects.
            PostQuitMessage(0);
            return 0;
        case WM_PAINT:
            // Paint the window's client area.
            onPaint(hWnd);
            return 0;

        case WM_KEYDOWN:
            switch (wParam) {
                case VK_LEFT:   // LEFT ARROW
                    position.x -= 1;
                    break;

                case VK_RIGHT:  // RIGHT ARROW
                    position.x += 1;
                    break;

                case VK_UP:   // UP ARROW
                    position.y += 1;
                    break;

                case VK_DOWN:  // DOWN ARROW
                    position.y -= 1;
                    break;
            }
            InvalidateRect(hWnd, NULL, TRUE);
            break;

        case WM_MOUSEMOVE:
            GetCursorPos(&mouse);
            ScreenToClient(hWnd, &mouse);
            InvalidateRect(hWnd, NULL, TRUE);
            break;
        //
        // Process other messages.
        //
        default:
            return DefWindowProc(hWnd, uMsg, wParam, lParam);
    }
}

void onPaint(HWND hWnd)
{
    PAINTSTRUCT ps;                 // information can be used to paint the client area of a window owned by that application
    HDC         hDC;                // device context
    HBRUSH brush, oldbrush;
    int x;
    int y;

    hDC = BeginPaint(hWnd, &ps);    // prepares the specified window for painting

    brush = CreateSolidBrush(RGB(47, 64, 54));
    oldbrush = SelectObject(hDC, brush);

    Ellipse(hDC, 200 + position.x, 200 - position.y, 220 + position.x, 220 - position.y);
    Ellipse(hDC, 220 + position.x, 200 - position.y, 240 + position.x, 220 - position.y);
    Ellipse(hDC, 240 + position.x, 200 - position.y, 260 + position.x, 220 - position.y);
    Ellipse(hDC, 260 + position.x, 200 - position.y, 280 + position.x, 220 - position.y);
    Ellipse(hDC, 280 + position.x, 200 - position.y, 300 + position.x, 220 - position.y);

    DeleteObject(brush);
    brush = CreateSolidBrush(RGB(90, 117, 102));
    SelectObject(hDC, brush);
    
    Rectangle(hDC, 200 + position.x, 180 - position.y, 300 + position.x, 200 - position.y);

    Rectangle(hDC, 220 + position.x, 165 - position.y, 280 + position.x, 180 - position.y);

    SelectObject(hDC,oldbrush);
    DeleteObject(brush);

    MoveToEx(hDC, 250 + position.x, 165 - position.y, NULL);
    //LineTo(hDC, 250 + mouse.x, 120 - mouse.y);
    x = mouse.x + position.x;
    y = mouse.y - position.y;
    LineTo(hDC, x, y);

    

    DeleteDC(hDC);                  // deletes the specified device context
    EndPaint(hWnd, &ps);            // marks the end of painting in the specified window
}